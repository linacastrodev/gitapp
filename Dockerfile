FROM node:latest
MAINTAINER Lina Castro <linacastrodev@gmail.com>
LABEL maintainer="Victor Huezo <huezohuezo1990"
ENV DEBIAN_FRONTEND noninteractive
WORKDIR /gitapp
ADD . /gitapp
RUN npm install
CMD npm run serve
